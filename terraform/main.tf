variable "pet_name_length" {
  type = number
}

resource "random_pet" "main" {
  length = var.pet_name_length
}

output "pet_name" {
  value = random_pet.main.id
}